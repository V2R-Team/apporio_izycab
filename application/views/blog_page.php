<?php 
       $site_lang =$this->session->userdata('site_lang');
       include('header.php'); ?>
 
<div id="main-content">
 
<section class="pt-50 pb-100 main_sect">
	<div class="container">
		<img class="blog_img" src="<?php echo base_url($page_data['image']);?>" />

		<h3 class="blog_title">
			<?php
				if($site_lang == 'french'){	
					echo $page_data['heading_arabic'];
				}
				else
				{
					echo $page_data['heading'];
				}
			?>

		</h3>
		
		<div class="blog_content">
			<?php 
				if($site_lang == 'french'){ 
					echo $page_data['content_arabic'];
				}
				else
				{
					echo $page_data['content'];
				}
			?>
		</div>
    </div>		
</section> 

     




 

<?php include('footer.php'); ?>