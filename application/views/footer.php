 <!-- <section><img src="<?php echo base_url();?>images/footer.png"/></section>-->
  </div>
   <footer class="footer">
    <section class="bottom-section">
      <div class="container">
        <div class="row">
          <div class="col-md-10">
            <nav class="footer-nav">
              <ul>
                <li><a href="<?php echo base_url();?>index.php/Welcome/about"><?php echo $this->lang->line('about_us');?></a></li>
                <li><a href="<?php echo base_url();?>index.php/Welcome/contact_us"><?php echo $this->lang->line('contact_us');?></a></li>
                
              </ul>
            </nav>
          </div>
          <div class="col-md-2">
           <nav class="footer-nav">
              <ul>
                
                <li><a href="" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></i></a></li>
                <li><a href="" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
              </ul>
            </nav>
          </div>
        </div>
		
	<div class="row">
          <div class="col-md-12">
           <center> <p style="color:white;"> © <?php echo $Get_homedata['web_footer']?></p></center>
             
          </div>
          
        </div>
		
		
      </div>
    </section>
  </footer>
</div>
<script type="text/javascript" src="<?php echo base_url();?>/js/jquery-1.11.3.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/js/custom-script.js"></script>
</body>

</html>