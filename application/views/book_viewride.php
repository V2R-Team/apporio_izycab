<?php $site_lang =$this->session->userdata('site_lang'); ?>
<!DOCTYPE html>
<html>
<title><?php echo $header_data['web_title'];?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url();?>webstatic/css/style1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/custom1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap1.css" type="text/css">
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>


.modal_lft{
    position:absolute !important;
    top:35%;
}
</style>
<style>
    .divmap{ height: 300px}
</style>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCDl845ZGSDvGV5BzufLsjRQC04JARqErg&sensor=false"></script>
<?php
$lat = $normal_ride['pickup_lat'];
$pickup_long = $normal_ride['pickup_long'];
$pickup_location = $normal_ride['pickup_location'];
$drop_lat = $normal_ride['drop_lat'];
$drop_long = $normal_ride['drop_long'];
$drop_location = $normal_ride['drop_location'];
?>
<script type="text/javascript">
    var markers = [
        {
            "title": 'Pick Up Location',
            "lat": '<?php echo $lat;?>',
            "lng": '<?php echo $pickup_long;?>',
            "description": '<?php echo $pickup_location;?>'
        }
        ,
        {
            "title": 'Drop Up Location',
            "lat": '<?php echo $drop_lat;?>',
            "lng": '<?php echo $drop_long;?>',
            "description": '<?php echo $drop_location;?>'
        }
    ];
    window.onload = function () {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var lat_lng = new Array();
        var latlngbounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            lat_lng.push(myLatlng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });
            latlngbounds.extend(marker.position);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(data.description);
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
        var path = new google.maps.MVCArray();
        var service = new google.maps.DirectionsService();


        var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });
        for (var i = 0; i < lat_lng.length; i++) {
            if ((i + 1) < lat_lng.length) {
                var src = lat_lng[i];
                var des = lat_lng[i + 1];
                path.push(src);
                poly.setPath(path);
                service.route({
                    origin: src,
                    destination: des,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                }, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                            path.push(result.routes[0].overview_path[i]);
                        }
                    }
                });
            }
        }
    }
</script>

<script>
 function getInfo(){

    $.ajax({
        type: "GET",
        url: "<?php echo base_url();?>index.php/Mail_invoice?id=<?php echo urlencode(base64_encode($normal_ride['ride_id']));?>&em=<?php echo urlencode(base64_encode($normal_ride['user_email']));?>&type=<?php echo urlencode(base64_encode(1));?>",
        
       data:{
        },
        success: 
        function(data){
       
     $('#myModal').modal({ show: 'true'})
			 
        }
    });
       
  };
  </script>
  <script>
 function getInfo1(){
 var email_id=document.getElementById("email_id").value;
 if(email_id == "")
 {
alert('Plz enter Email id First');
return false;
 }
 var ride_id=document.getElementById("ride_id").value;
 var type=document.getElementById("type").value;
  $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Mail_invoice/new_mail",
       data: {
				email_id : email_id,
				ride_id : ride_id,
				type : type,
			 } 
			 ,
        success: 
        function(data){
           $('#myModal').modal({ show: 'true'})
        }
    });
}

</script> 
  

<body class="main_bg">

<!-- Sidebar -->
 
<!-- Page Content -->
<div class="main_dv">
 <div class="lft_side">
 
 
 
 
      <div class="modal fade modal_lft" id="myModal" role="dialog">
            <div class="modal-dialog" style="width: auto; !important">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <center>  <h4 class="modal-title"><?php echo $this->lang->line('mailsuccess');?></h4>  </center>
                    </div>
                    <div class="modal-footer">
                      <center> <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('mailclose');?></button></center>
                    </div>
                </div>
                   </div>
                </div>

	 <div class="modal fade modal_lft" id="myModal1" role="dialog">
            <div class="modal-dialog" style="width: auto; !important">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <center>  <h4 class="modal-title"><?php echo $this->lang->line('mailinvoice');?></h4>  </center>
                       <div class="input-group" style="width: 100%; !important">
                        <input  type="text" name="email_id" id="email_id" class="form-control newsignup" style="background-color:#FFF; width: 100%; !important;" placeholder="<?php echo $this->lang->line('mailinvoice1');?>" required>
						<input  type="hidden" name="ride_id" id="ride_id" value="<?php echo $normal_ride['ride_id'];?>">
						<input  type="hidden" name="type" id="type" value="1">
					 </div>
                    </div>
                    <div class="modal-footer">
                     <center> <button onclick='getInfo1();' type="submit" class="btn btn-default" data-dismiss="modal" style="width: 100%; !important"><?php echo $this->lang->line('sendinvoice');?></button></center>
                    </div>
                </div>
                   </div>
                </div>		


  <div class="modal fade modal_lft" id="myModalcancel" role="dialog">
            <div class="modal-dialog" style="width: auto; !important">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <center>  <h4 class="modal-title"><?php echo $this->lang->line('selectcancel');?></h4>  </center>
						<form name="myForm" action="<?php echo base_url();?>index.php/Booking_controller/cancel_booking1" method="post">
                       <div class="input-group" style="width: 100%; !important">
						 <select class="form-control newsignup" name="reason_id" id="reason_id"  style="background-color:#FFF; width: 100%; !important;" required >
						   <?php foreach($cancel_reason  as $reason ):?>
                         <option value="<?= $reason['reason_id'] ?>"><?= $reason['reason_name'] ?></option>
                        <?php endforeach; ?>
					</select> 
						<input  type="hidden" name="ride_id" id="ride_id" value="<?php echo $normal_ride['ride_id'];?>">
					 </div>
                    </div>
                    <div class="modal-footer">
			<center>     <button  type="" class="btn btn-default" data-dismiss="modal" style="width: 40%; !important"><?php echo $this->lang->line('mailclose');?></button> 
                 <button  type="submit" class="btn btn-default"  style="width: 40%; !important"><?php echo $this->lang->line('cancelride');?></button>	</center> 
                    </div>
					</form>
                </div>
                   </div>
                </div>	
 
 
   
    <div class="modal fade" id="myModal21" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $this->lang->line('callbelow');?></h4>
        </div>
        <div class="modal-body">
          <p><?php echo $normal_ride['driver_phone'];?></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('mailclose');?></button>
        </div>
      </div>
      
    </div>
  </div>
  
  
  
  <div class="tp_header1">
  <div class="col-md-2 col-sm-2 col-xs-3 tp_menu">
      <a class="bnr_a" href="<?php echo base_url();?>index.php/Booking_controller/user_profile">
         <img src="<?php echo base_url();?>images/back1.png" width="19"/>
      </a>
  </div>

 

      <div class="col-md-8 col-sm-8 col-xs-6 tp_logo_txt">CRN <?php echo $normal_ride['ride_id'];?></div>
       <div class="col-md-2 col-sm-2 col-xs-2"></div>

<div class="clear"></div>
</div>


       
	 <div class="ride_status_time" style="text-align:center">
     
     <span> <?php $ride_status=$normal_ride['ride_status'];
                                                         switch ($ride_status){
                                                         case "1":
                                                            if( $normal_ride['ride_type'] == '2') {
                                                              echo  "<font color='green'>". $this->lang->line('Scheduled')." - ".$normal_ride['later_date']."</font>";
                                                              }
                                                              else
                                                              {
                                                              echo ("<font color='green'>". $this->lang->line('New_Booking')." </font>\n ".$timestap);
                                                              }
                                                            break;
                                                        case "2":
                                                            echo ("<font color='red'>". $this->lang->line('Cancelled_By_User')." </font>   ".$timestap);
                                                            break;
                                                        case "3":
                                                            echo ("<font color='green'>". $this->lang->line('Accepted_by_Driver')." </font>   ".$timestap);
                                                            break;
                                                        case "4":
                                                            echo nl2br("<font color='red'>". $this->lang->line('Cancelled_by_driver')." </font>    ".$timestap);
                                                            break;
                                                        case "5":
                                                            echo ("<font color='green'>". $this->lang->line('Driver_Arrived')." </font>    ".$timestap);
                                                            break;
                                                        case "6":
                                                            echo ("<font color='green'>". $this->lang->line('Trip_Started')." </font>    ".$timestap);
                                                            break;
                                                        case "7":
                                                            echo ("<font color='green'>". $this->lang->line('Trip_Completed')."  </font>   ".$timestap);
                                                            break;
                                                        case "8":
                                                            echo ("<font color='green'>". $this->lang->line('Trip_Book_By_Admin')." </font>    ".$timestap);
                                                            break;
							case "9":
                                                            echo nl2br("<font color='red'>". $this->lang->line('Cancelled_by_driver')." </font>    ".$timestap);
                                                            break;
                                                        case "17":
                                                            echo ("<font color='red'>". $this->lang->line('Trip_Cancel_By_Admin')." </font>    ".$timestap);
                                                            break;
                                                        default:
                                                            echo "----";
                                                    }
                                                    ?> - <?php echo $normal_ride['last_time_stamp']; ?> </span>
  
     </div>   
 <div class="list-group">
  <div class="panel panel-default">
	<div id="dvMap" class="divmap col-md-12"></div>
	<div class="clear"></div>
  </div>
</div>
 <div class="car_type_full_dlts" style="margin-top:30px;">
  
  
  
     <div class="car_type_dtls">
		<div class="car_type_img">
			<img src="<?php echo base_url($normal_ride['car_type_image']);?>" width="40" height="40" alt="">
		</div>
		
		<div class="car_nm_desc2">
			<?php if($normal_ride['ride_type'] == '1') { ?> 
				<h3 style="margin-top: 5px !important;" class="ride_date_time">
					<?php if($site_lang == 'french'){ echo $normal_ride['car_name_arabic']; } else { echo $normal_ride['car_type_name'];}?>
				</h3>
			<?php } elseif($normal_ride['car_model_name'] == "") { ?>
				<h3 style="margin-top: 5px !important;" class="ride_date_time">
					<?php if($site_lang == 'french'){ echo $normal_ride['car_name_arabic']; } else { echo $normal_ride['car_type_name'];}?> 
				</h3>
			<?php } else {?>
				<h3 style="margin-top: 5px !important;" class="ride_date_time"><?php echo $normal_ride['car_model_name'];?>  </h3>
			<?php } ?>
			<?php if($ride_status ==1 ) { ?>
				<p class="crn_number"><?php echo $this->lang->line('later_ride');?> </p>
			<?php }?>
	  
       </div>
	   <div class="clear"></div>
	   </div>
	   
	   
	   
	   
	   <div class="car_type_dtls">
	 
		 <div class="loc_main1">
			<div class="loc_pic_drop"><?php echo $this->lang->line('pickup');?></div>
				<div class="loc_from_to1"><?php echo $normal_ride['pickup_location'];?></div>
			<div class="clear"></div>
		 </div>	
	  	 
		 <br>
			<div class="loc_main1">
				  <div class="loc_pic_drop"><?php echo $this->lang->line('drop');?></div>
			  <div class="loc_from_to1"> <?php  echo $normal_ride['drop_location'];?> </div>
			</div>
	<div class="clear"></div>
     </div>
	   
	   
	   
	
	
	   <?php if($normal_ride['driver_name'] != "") {?>
	   <?php if($normal_ride['ride_type'] != '1' || $normal_ride['ride_status'] == '3' || $normal_ride['ride_status'] == '5' || $normal_ride['ride_status'] == '6' || $normal_ride['ride_status'] == '8' || $normal_ride['ride_status'] == '7'  ) { ?> 
		<div class="car_type_dtls">
		<div class="car_type_img">
			<?php if( $normal_ride['driver_image'] != "") { ?>
				<img src="<?php echo base_url($normal_ride['driver_image']);?>" width="40" height="40" alt="">			
			<?php } else { ?>
			 
				<img src="http://soul-fi.ipn.pt/wp-content/uploads/2014/09/user-icon-silhouette-ae9ddcaf4a156a47931d5719ecee17b9.png" width="40" height="40" alt="">
			<?php }?>
	   </div>
	   <div class="car_nm_desc2">
			<h3 style="margin-top: 5px !important;" class="ride_date_time"><?php echo $normal_ride['driver_name'];?> </h3>	  
       </div>
	      <div class="clear"></div>
	   </div>
	   <?php } } ?>
	   
	   
	   
<?php if($normal_ride['total_amount'] != 0 ) { ?>
   <div class="car_type_dtls">
   	  
   	  <div class="car_type_img">
      	  	<img src="<?php echo base_url();?>webstatic/speed-meter.png" width="40" height="40" alt="">
      	  </div>
	  
	  <div class="car_nm_desc2">
	  	<p style="margin-top:11px !important;">
	  	    <span class="spd_mtr_content"> <?php echo $normal_ride['total_amount'];?><?php echo app_currency;?> </span>
	  	    <span class="spd_mtr_content"><?php echo $normal_ride['distance'];?></span>
	  	    <span class="spd_mtr_content">
	  	    	<?php 
					$checkTime = strtotime($normal_ride['arrived_time']); 
					//echo date('H:i:s', $checkTime);
					$loginTime = strtotime($normal_ride['end_time']);
					$diff = $checkTime - $loginTime;
		
					//echo round(abs($diff)/60)." min";
					echo round(abs($time)/60)." min"; ?>
		    </span>
	        </p>
         </div>
	   <div class="clear"></div>
    </div>
 <?php } ?>
	   
	   
	   
	   
	    
	   
	   
	   <?php if($normal_ride['driver_name'] != "") {?>
    <?php  if($normal_ride['ride_status'] == '3') { ?>   
     <a data-target="#myModal21" data-toggle="modal" id="myModal21"  class="bnr_a">
	    <div class="car_type_dtls">
			<div class="car_type_img">
				<img src="https://cdn4.iconfinder.com/data/icons/rcons-phone/16/handset_round-2-256.png" width="40" height="40" alt="">
			</div>
			
			<div class="car_nm_desc2">
				<h3 class="ride_date_time" style="margin-top:14px !important;"><?php echo $this->lang->line('calldriver');?>  </h3>
			</div>
			<div class="clear"></div>
		</div> 
     </a>
     <?php } } ?>   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	 
	 
	   <?php if($normal_ride['ride_status'] == 7 ) { 
	   if($normal_ride['user_email'] != ""){ ?> 
			<a href="#" onclick='getInfo();' class="bnr_a">
			
			<div class="car_type_dtls"> 
				<div class="car_type_img">
					<img src="http://icons.iconarchive.com/icons/iconsmind/outline/512/Mail-icon.png" width="40" height="40" alt=""/>
				</div>
				<div class="car_nm_desc2">
					<h3 class="ride_date_time" style="margin-top:14px !important;"><?php echo $this->lang->line('mailinvoice');?> </h3>
				</div>
				<div class="clear"></div>
			</div>
	      <div class="clear"></div>
     </a>
	   <?php } else {  ?>
         
		   <a href="#"  data-toggle="modal" data-target="#myModal1" class="bnr_a">
		    <div class="car_type_dtls"> 
				<div class="car_type_img">
						<img src="http://icons.iconarchive.com/icons/iconsmind/outline/512/Mail-icon.png" width="40" height="40" alt="">
				</div>
				<div class="car_nm_desc2">
					<h3 class="ride_date_time" style="margin-top:14px !important;"><?php echo $this->lang->line('mailinvoice');?> </h3>
		 	    </div>
				<div class="clear"></div>
			</div>
         </a>
	   <?php } } ?>
	   
	   
	   
	 
     <a href="<?php echo base_url();?>index.php/Welcome/contact_us" target="_blank" class="bnr_a">
		<div class="car_type_dtls"> 
			<div class="car_type_img">
				<img src="https://maxcdn.icons8.com/Share/icon/dotty/Business//online_support1600.png" width="40" height="40" alt="">
			</div>
			<div class="car_nm_desc2">
					<h3 class="ride_date_time" style="margin-top:14px !important;"><?php echo $this->lang->line('suppourt_ride');?>  </h3>
			</div>
			<div class="clear"></div>
		</div>
     </a>
	 
	 
    <?php if($normal_ride['ride_status'] == '1' || $normal_ride['ride_status'] == '3' ) { ?> 
 
   <a href="" onClick="checkInputs()" data-toggle="modal" data-target="#myModalcancel" class="bnr_a">
   	   <div class="car_type_dtls"> 
			<div class="car_type_img">
				<img src="https://maxcdn.icons8.com/Share/icon/Very_Basic/cancel1600.png" width="40" height="40" alt="">
			</div>
			<div class="car_nm_desc2">
					<h3 class="ride_date_time" style="margin-top:14px !important;"><?php echo $this->lang->line('cancelride');?></h3>
			</div>
			<div class="clear"></div>
		</div>
    </a>
     <?php } ?>
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
   
   </form>      
		
     </div>
	    <div class="clear"></div>
     </div>
	 
	 
	 
	 
	 
	 
	 
    
	 
	 
     
      
 
</div>


  
  
 
 
   
  <div class="web_lang hidden-xs">
                  <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                                    <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>French</option>
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                </select> 
</div>

</body>
</html> 
