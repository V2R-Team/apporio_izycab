 
				 
 <?php if($this->session->flashdata('passsuccess')):?>
     <script>alert("Password Succesfully Changed!!");</script>
<?php unset($_SESSION['passsuccess']); endif; ?>

 <?php if($this->session->flashdata('passfail')):?>
     <script>alert("Wrong Old Password!!");</script>
<?php unset($_SESSION['passfail']);  endif; ?>

 <?php if($this->session->flashdata('new_password')):?>
     <script>alert("Password & Confirm Password does not match!!");</script>
	 
<?php unset($_SESSION['new_password']);  endif; ?>

 <?php if($this->session->flashdata('passmatchsuccess')):?>
     <script>alert("Old password & New Password are same!!");</script>
	 
<?php unset($_SESSION['passmatchsuccess']);  endif; ?>


<?php include('header_driver.php'); ?>

<script>
function validateForm() {
    var x = document.forms["myForm"]["old_password"].value;
	var y = document.forms["myForm"]["new_password"].value;
	var z = document.forms["myForm"]["confirm_password"].value;
 
	 
	 
    if (x == "") {
        alert("Please Enter Old Password");
        return false;
    }
	 else if (y == "") {
        alert("Please Enter New Password");
        return false;
    }
	else if (z == "") {
        alert("Please Enter Confirm Password");
        return false;
    }
	
}
</script>
  <section class="mt-30">
    <div class="container">
      <div class="row mt-60 mb-100 pt-20 pb-20 profile_content">
        <div class="tabs-vertical-env">
          <div class="col-md-3 hidden-xs">
            <ul class="nav tabs-vertical left_tab">
              <?php if ($driver_image == "") { ?>
              <li class="driver_profile"> <img class="col-md-4 col-sm-4 col-xs-4"  src="<?php echo base_url();?>/images/profile.png"/>
                <?php } else{?>
                 <li class="driver_profile"> <img    class="col-md-4 col-sm-8 col-xs-8" src="<?php echo base_url($driver_image); ?>">
                <?php } ?>
                <div class="col-md-8 col-sm-8 col-xs-8 driver_info">
                  <div class="driver_name"><?php echo $driver_name; ?></div>
                  <div class="driver_car_name"><?php echo $car_model_name; ?></div>
                  <div class="driver_car_number"><?php echo $car_number; ?></div>
                  <div class="driver_rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> </div>
                </div>
                <div class="clear"></div>
                	<?php if($login_logout == 1) { ?>
                <button class="btn btn-success btn-xs online_offline"><?php echo $this->lang->line('online');?></button>
				<?php } else { ?>
				  <button   class="btn btn-danger btn-xs online_offline"><?php echo $this->lang->line('offline');?></button>
				<?php } ?>
                <div class="clear"></div>
              </li>
              <li class=""> <a href="<?php echo base_url();?>index.php/Welcome/driver_profile"><?php echo $this->lang->line('drprofile');?><i class="fa fa-user" aria-hidden="true"></i></a> </li>
              <li class="">  <a href="<?php echo base_url();?>index.php/Welcome/driver_rides"><?php echo $this->lang->line('drmyrides');?><i class="fa fa-car" aria-hidden="true"></i></a> </li>
              <!--<li class=""> <a href="driver-earnings.php">Earnings <i class="fa fa-usd" aria-hidden="true"></i></a> </li>-->
              <li class=""> <a href="<?php echo base_url();?>index.php/Welcome/driver_documents"><?php echo $this->lang->line('drdocument');?><i class="fa fa-file-text" aria-hidden="true"></i></a> </li>
              <li class="active">  <a href="<?php echo base_url();?>index.php/Welcome/driver_password"><?php echo $this->lang->line('drchpass');?><i class="fa fa-lock" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="<?php echo base_url();?>index.php/Welcome/driver_suppourt"><?php echo $this->lang->line('drsupport');?><i class="fa fa-life-ring" aria-hidden="true"></i></a> </li>
             <li class=""> <a href="<?php echo base_url();?>index.php/Welcome/about"><?php echo $this->lang->line('drabout');?> <i class="fa fa-info" aria-hidden="true"></i></a> </li>
             <!--  <li class=""> <a href="terms.php">Terms & Conditions<i class="fa fa-cogs" aria-hidden="true"></i></a> </li>-->
              <li class="">  <a href="<?php echo base_url();?>index.php/Welcome/logout"><?php echo $this->lang->line('drlogout');?><i class="fa fa-sign-out" aria-hidden="true"></i></a> </li>
            </ul>
          </div>
          <div class="tab-content col-md-9 tab_content">
            
            
            <div class="" id="">
              <h4 class="pb-30"><?php echo $this->lang->line('drchangepass');?></h4>
              <div class="col-md-6">
               <form name="myForm" action="<?php echo base_url();?>index.php/Useraccounts/update_driver_password" onsubmit="return validateForm()" method="post">
                  <div class="form-group edit_profile_label">
                    <label><?php echo $this->lang->line('drchangeoldpass');?></label>
                    <input type="password" name="old_password" class="form-control edit_profile_field" id="old_password" placeholder="<?php echo $this->lang->line('drchangeoldpass');?>" required>
                  </div>
                  <div class="form-group edit_profile_label">
                    <label><?php echo $this->lang->line('drchangenewpass');?></label>
                    <input type="password" name="new_password" class="form-control edit_profile_field" id="new_password" placeholder="<?php echo $this->lang->line('drchangenewpass');?>" required>
                  </div>
                  <div class="form-group edit_profile_label">
                    <label><?php echo $this->lang->line('drchangeconpass');?></label>
                    <input type="password" name="confirm_password" class="form-control edit_profile_field" id="confirm_password" placeholder="<?php echo $this->lang->line('drchangeconpass');?>" required>
                  </div>
                  <div class="form-group">
                    <input type="submit" class="submit_btn" style="width:100%;" value="<?php echo $this->lang->line('drchangepass');?>">
                  </div>
                </form>
              </div>
              <div class="col-md-6"></div>
            </div>
            
            
            
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php include('footer.php'); ?>
