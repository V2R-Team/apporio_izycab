<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*******FRENCH_LANGUAGE*******/
/******************************/
	/*****Headers**********/
		$lang['sign'] = 'Se connecter';
		$lang['become_driver'] = 'BECOME A DRIVER';
		$lang['book_now'] = 'RESERVER MANTENANT';
		$lang['logout'] = 'Déconnexion';
		$lang['dashboard'] = 'Tableau de bord';
		$lang['myrides'] = 'Mes trajets';
		
		
	/** ratecard**/
	$lang['ratecard'] = 'Sélectionner une ville';
	$lang['ratecard1']='Sélectionner un véhicule';
	$lang['ratecard3'] = 'Frais distance de trajet';
	$lang['ratecard4']='Tarif minimum';
	$lang['ratecard5'] = 'Au delà de la distance minimum';
	$lang['ratecard6']='pour';
	$lang['ratecard7'] = 'Kms';
	$lang['ratecard8']='Frais temps de trajet';
	$lang['ratecard9'] = 'Temps de trajet gratuit';
	$lang['ratecard10']='Après le temps de trajet gratuit';
	$lang['ratecard11'] = 'Frais temps dattente';
	$lang['ratecard12']='Temps dattente gratuit';
	$lang['ratecard13'] = 'Frais après temps dattente gratuit';
	$lang['ratecard14']='Frais supplémentaires';
	$lang['ratecard15'] = 'Frais heures de pointe';
	$lang['ratecard16']='Temps dattente gratuit';

	/******Index_page_start***********/
  	/*****Index_BannerImage**********/
	  
	    $lang['bannertxt1'] = 'Réserver un Izycab pour vos trajets';
		$lang['bannertxt2'] = 'Chosissez parmi les types de véhicules ou une lise a disposition';
        $lang['bnrsearchbox1'] = 'Votre partenaire pour vos trajets quotidiens'; 
        $lang['bnrsearchbox2'] = 'Une flotte de véhicule dédiée';

    /***********form data************/	
    	$lang['cityheader'] = 'RESERVER UN TRAJET';	
		$lang['rentalheader'] = 'MISE A DISPOSITION';
        $lang['pickup'] = 'Départ';	
		$lang['drop'] = 'Arrivée';
		$lang['when'] = 'Quand';
		$lang['depart'] = 'DEPART';
		$lang['enterpick'] = 'Saisir votre adresse de déapart';
		$lang['enterdrop'] = 'Saisir votre destination pour une estimation du prix';
		$lang['schlater'] = 'Réserver à lavance';
		$lang['now'] = 'Maintenant';
		$lang['searchbtn'] = 'Rechercher des trajets';
        
 
    /*****Index_Footer**********/
     
        $lang['Home'] ='Accueil';
        $lang['about_us'] ='A propos de nous';
        $lang['contact_us'] ='Contactez nous';
	    $lang['iphone'] ='Disponible sur';
		$lang['android'] ='Disponible sur';
        $lang['iphone1'] ='Apple strore';
		$lang['android1'] ='Playstore';   
        
   
        
    /**************Sign_in_page************/
          $lang['driver_desc'] = 'Find everything you need to track your success on the road.';
          $lang['rider_desc'] = 'Choisissez vos options de paiement, Revoyez vos trajets passées, et plus encore.';
          $lang['driver_signin'] = 'Connexion chauffeur';
          $lang['rider_signin'] = 'Connexion Client ';
          
  /**************DriverSignUp page************/
          $lang['driversignup'] = 'Driver Sign up';
          $lang['firstname'] = 'First Name';
		  $lang['lastname'] = 'Last Name';
		  $lang['youremail'] = 'Your Email';
		  $lang['yourpass'] = 'Your password';
		  $lang['selectcity'] = 'Please Select City';
		  $lang['selectcar'] = 'Please Select Car Type';
		  $lang['selectmodel'] = 'Please Select Model';
		  $lang['carnumber'] = 'Car Number';
		  $lang['ridetype'] = 'Please Select Type of Ride';
		  $lang['signup'] = 'SIGN UP';
		  $lang['alreadysignin'] = 'Already have an account ?';
          $lang['normal'] = 'Normal Ride';
		  $lang['rental'] = 'Rental';
		  $lang['both'] = 'Both';  
		  
  /**************DriverSignIN_in_page************/
          $lang['driversignin'] = 'Driver Sign In';
          $lang['entermob'] = 'Enter your mobile number';
		  $lang['enteremail'] = 'Enter your email ID';
		  $lang['password'] = 'Password';
		  $lang['next'] = 'Next';
		  $lang['dontsignup'] = 'Dont have an account ?';
  /**************DriverProfile_page************/
          $lang['drprofile'] = 'Profile';
          $lang['drmyrides'] = 'My Rides';
		  $lang['drdocument'] = 'Documents';
		  $lang['drchpass'] = 'Change Password';
		  $lang['drsupport'] = 'Customer Support';
		  $lang['drabout'] = 'About us';
		  $lang['drlogout'] = 'Logout';
		  $lang['drName'] = 'Name';
		  $lang['drName1'] = 'Your Name';
          $lang['dremail'] = 'Email';
		  $lang['dremail1'] = 'Your Email';
		  $lang['drphone'] = 'Phone Number';
		  $lang['drphone1'] = 'Your Phone Number';
		  $lang['updatepro'] = 'Update Profile';
		  $lang['online'] = 'Online';
		  $lang['offline'] = 'Offline';
		  
		  
  /**************DriverRides_page************/
          $lang['myridesprofile'] = 'Profile';
          $lang['myridesmyride'] = 'My Rides';
		  $lang['myridesridername'] = 'Rider Name';
		  $lang['myridesphone'] = 'Phone';
		  $lang['myridesdate'] = 'Date';
		  $lang['myridesstatus'] = 'Status';
		  
  /**************DriverDocuments_page************/
          $lang['dryourdocs'] = 'Your Documents';
       
  /**************DriverChangePassword_page************/
          $lang['drchangepass'] = 'Change Password';	
          $lang['drchangeoldpass'] = 'Old Password';	
		  $lang['drchangenewpass'] = 'New Password';	
		  $lang['drchangeconpass'] = 'Confirm Password';	  		  
	
  /**************User Sign IN_page************/
          $lang['usrloginmob'] = 'Saisir votre numéro mobile';	
          $lang['usrloginnxt'] = 'Suivant';
          $lang['newuser'] = 'Nouvel utilisateur cliquez ici ...';
          $lang['enteruserpass'] = 'Tapez votre mot de passe';

  /**************User Sign UP_page************/
          $lang['usrsigninacc'] = 'Créer votre compte Izycab';	
          $lang['usrsigninacc1'] = 'Saisir vos informations pour créer un compte';	
          $lang['usrsigninfullname'] = 'Saisir votre nom et prénom';	
          $lang['usrsigninemail'] = 'Saisir adresse email(facultatif)';	
          $lang['usrsigninsave'] = 'Enregister';	
          $lang['usrsigninterms'] = 'En poursuivant je reconnais avoir lu et accepét les CGU';	
          $lang['usrsigninterms1'] = 'CGU';		


  /**************Normal Booking Main Page************/
          $lang['normalfrom'] = 'Départ';	
          $lang['normalto'] = 'Destination';	
          $lang['normalwhen'] = 'Quand';	
          $lang['normalavaliable'] = 'Chauffeurs disponibles ';	
	  $lang['normallogin'] = 'CONNEXION';
	  $lang['normalloginenterloc'] = 'Veuillez entrer lemplacement';
          $lang['normalloginenterloc1'] = 'pour obtenir lestimation des tarifs.';
          $lang['normalnodriver'] = 'Aucun chauffeur disponible pour le moment. réessayez dans un moment.';
     			  

  /**************Rental Booking Main Page************/
          $lang['rentalfrom'] = 'Départ';	
          $lang['rentalto'] = 'Destination';	
          $lang['rentalwhen'] = 'Quand';	
          $lang['rentalavaliable'] = 'Chauffeurs disponibles ';	
		  $lang['rentallogin'] = 'CONNEXION';
		  $lang['rentalpkg'] = 'PACK';
		  $lang['rentalselpkg'] = 'Selectionner un pack';
     			  


  /**************Navigation Bar Page************/
          $lang['navbarhome'] = 'Accueil';	
          $lang['navbarbookride'] = 'Reserver un trajet';	
          $lang['navbaryourride'] = 'Mes trajets';	
          $lang['navbarratecard'] = 'Grille tarifaire';	
		  $lang['navbarsupport'] = 'Support';
		  $lang['navbarappdown'] = 'Téléchargez lapplication';

  /**************Normal Booking Location Page************/
          $lang['normallocfrom'] = 'Enter Pickup Location ';	
          $lang['normallocto'] = 'Enter Drop Location';	
        

  /**************Rental Booking Location Page************/
          $lang['rentallocfrom'] = 'Saisir lieu de départ';	
          $lang['rentallocto'] = 'Saisir destination';	
          $lang['select_ package'] = 'Veuillez sélectionner un forfait en premier';	
        		 
  /**************User Profile Page************/
          $lang['useryourprofile'] = 'MOn profil';	
          $lang['userseeallridesprofile'] = 'Voir tous les trajets';
          $lang['userseelogoutprofile'] = 'Déconnexion';
          $lang['userseenoridesprofile'] = 'Vous navez pas encore de trajets ..';
         $lang['userseeshowconfirmprofile'] = 'Etes vous sûr de vouloir vous déconnecter ?';
		  
  /**************User All Trips Page************/
          $lang['userallridesyourride'] = 'Mes trajets';	
		  $lang['userallridesnoride'] = 'Pas de trajets jusqua présent , Réservez maintenant..';
         

  /**************BookingConfirm pages Normal************/
          $lang['bookingpick'] = 'Départ';	
		  $lang['bookingdrop'] = 'Lieu darrivée';	
		  $lang['bookingfare'] = 'Tarif estimé';
		  $lang['bookingfare1'] = '(Les  tarifs sont plus elevés que dhabitude)';
		  $lang['bookingpayby'] = 'Payer par';
          $lang['bookingcoupon'] = 'Code promo';
		  $lang['bookingcoupon1'] = 'Saisir code promo(Facultatif)';	
          $lang['bookingmodel1'] = 'Réservation confirmé';	
		  $lang['bookingmodel2'] = 'Vous allez recevoir les détais de votre chaffeur 15mn avant le départ.';		  
		  $lang['bookingcofirm'] = 'Confirmer réservation';			  
		  $lang['bookingpickupon'] = 'Lieu de départ';		

  /**************BookingConfirm pages Rental************/
          $lang['bookingrentalpick'] = 'Départ';	
		  $lang['bookingrentalpayby'] = 'Payer par';
		  $lang['bookingrentalcoupon'] = 'Code promo';
		  $lang['bookingrentalcoupon1'] = 'Saisir code promo(Facultatif)';
		  $lang['bookingrentalmodel1'] = 'Réservation confirmé';	
		  $lang['bookingrentalmodel2'] = 'Vous allez recevoir les détais de votre chaffeur 15mn avant le départ.';		  
		  $lang['bookingrentalcofirm'] = 'Confirmer réservation';			  
		  $lang['bookingrentalpickupon'] = 'Lieu de départ';			  

		  $lang['bookingrentalhour'] = 'Heures';	
		  $lang['bookingrentalmiles'] = 'Km';
		  $lang['bookingrentalmiles1'] = 'Km';
		  $lang['bookingrentalbasefare'] = 'Tarif de base';
		  $lang['bookingrentalincludes'] = 'Comprend';	
		  $lang['bookingrentaladditionaldis'] = 'Tarif disptance suppléménaire';	
          $lang['bookingrentaladditionaltime'] = 'Tarif temps de trajet supplémentaire';			  
		  $lang['bookingrentalafterfirst'] = 'Après premier';			  
		 	
  /**************Location From Map Page************/
          $lang['maplocation'] = 'Déplacer la carte pour ajuster la position';	
		  $lang['maplocationconfirm'] = 'Confirmer position';	
		  
		  
  /**************Book View Ride************/
          $lang['mailsuccess'] = 'Email envoyé avec succès dans votre boite mail.';	
		  $lang['mailclose'] = 'Fermer';	
		  $lang['mailinvoice'] ='Email facture';
		  $lang['mailinvoice1'] = 'Saisir votre email';	
		  $lang['sendinvoice'] = 'envoyer facture';	
		  $lang['mailinvoice'] ='email facture';
		  $lang['selectcancel'] ='Selectionner la raison';
		  $lang['cancelride'] = 'Annuler trajet';	
		  $lang['suppourt_ride'] = 'Support';	
		  $lang['calldriver'] = 'Appeler chauffeur';	
		  $lang['callbelow'] = 'Appeler sur ce numéro';	
		  $lang['later_ride'] ='Vous allez recevoir les détais de votre chaffeur 15mn avant le départ. '; 
		  
		  
   /*************No Drivers Page************/
          $lang['noridesorry'] = 'D2solé pas de chaffeurs à proximité';	
		  $lang['noridegoback'] = 'Retour';	

   /*************Statuses************/
          $lang['Scheduled'] = 'Réservé';	
		  $lang['New_Booking'] = 'Nouvelle reservation';	
		  $lang['Cancelled_By_User'] = 'Annulé par le client';	
		  $lang['Accepted_by_Driver'] = 'Accepté par le chaffeur';	 
		  $lang['Cancelled_by_driver'] = 'Annulé par le chauffeur';	
		  $lang['Driver_Arrived'] = 'Votre chauffeur est arrivé';	
		  $lang['Trip_Started'] = 'Démarrage trajet';	
		  $lang['Trip_Completed'] = 'Fin de trajet';	 
		  $lang['Trip_Book_By_Admin'] = 'Trajet reservé par ladmin';	
		  $lang['Trip_Cancel_By_Admin'] = 'Trajet annulé par ladmin';	 
		  $lang['Trip_Reject_By_Driver'] = 'Trajet rejetté par le chauffeur';	
		  $lang['Trip_Cancel_By_User'] = 'Trajet annulé par le client';
          $lang['Trip_Reject_By_User'] = 'Trajet Rejetté par le client';		  
		  
		  
		  

 
		  		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		
		  
		  