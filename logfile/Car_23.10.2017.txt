Rental - Car Type Api: October 23, 2017, 2:27 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => PRESTIGE
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [base_fare] => 2500 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 121
            [base_fare] => 100 Per 4 Miles
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => Mini
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 121
            [base_fare] => 80 Per 3 Miles
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Dakar
latitude: 14.746504924491019
longitude: -17.520090378820896
-------------------------
Rental - Car Type Api: October 23, 2017, 2:29 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => PRESTIGE
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [base_fare] => 2500 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 121
            [base_fare] => 100 Per 4 Miles
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => Mini
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 121
            [base_fare] => 80 Per 3 Miles
            [ride_mode] => 1
        )

    [3] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Valenton
latitude: 48.76348395780183
longitude: 2.4469538033008575
-------------------------
Rental - Car Type Api: October 23, 2017, 3:49 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => PRESTIGE
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [base_fare] => 2500 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 121
            [base_fare] => 100 Per 4 Miles
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => Mini
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 121
            [base_fare] => 80 Per 3 Miles
            [ride_mode] => 1
        )

    [3] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Valenton
latitude: 48.76348395780183
longitude: 2.4469538033008575
-------------------------
Rental - Car Type Api: October 23, 2017, 3:50 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => PRESTIGE
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [base_fare] => 2500 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 121
            [base_fare] => 100 Per 4 Miles
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => Mini
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 121
            [base_fare] => 80 Per 3 Miles
            [ride_mode] => 1
        )

    [3] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Vincennes
latitude: 48.842945607924385
longitude: 2.431395649909973
-------------------------
Rental - Car Type Api: October 23, 2017, 7:14 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => PRESTIGE
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [base_fare] => 2500 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 121
            [base_fare] => 100 Per 4 Miles
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => Mini
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 121
            [base_fare] => 80 Per 3 Miles
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Dakar
latitude: 14.73913434373487
longitude: -17.509906366467476
-------------------------
Rental - Car Type Api: October 23, 2017, 10:04 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => PRESTIGE
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [base_fare] => 2500 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 121
            [base_fare] => 100 Per 4 Miles
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => Mini
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 121
            [base_fare] => 80 Per 3 Miles
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Dakar
latitude: 14.746513354539065
longitude: -17.520042099058628
-------------------------
Rental - Car Type Api: October 23, 2017, 11:39 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => PRESTIGE
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [base_fare] => 2500 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 121
            [base_fare] => 100 Per 4 Miles
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => Mini
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 121
            [base_fare] => 80 Per 3 Miles
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Dakar
latitude: 14.746513354539065
longitude: -17.520042099058628
-------------------------
Rental - Car Type Api: October 23, 2017, 11:41 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => PRESTIGE
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [base_fare] => 2500 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 121
            [base_fare] => 100 Per 4 Miles
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => Mini
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 121
            [base_fare] => 80 Per 3 Miles
            [ride_mode] => 1
            [currency_iso_code] => FCFA
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Dakar
latitude: 14.746513354539065
longitude: -17.520042099058628
-------------------------
