<?php
error_reporting(0);
header("Content-Type: application/json");
include_once '../apporioconfig/start_up.php';
$query = "select * from admin_panel_settings WHERE admin_panel_setting_id=1";
$result = $db->query($query);
$admin_settings = $result->row;
$latitude=$_REQUEST['latitude'];
$longitude=$_REQUEST['longitude'];
if($latitude !="" && $longitude !="" )
{

$geocode=file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&key='.$admin_settings['admin_panel_map_key']);
   $output= json_decode($geocode);
   foreach ($output->results as $result) {
		foreach($result->address_components as $addressPart) {
		  if ((in_array('locality', $addressPart->types)) && (in_array('political', $addressPart->types)))
		  $city = $addressPart->long_name;
	    	
		}
	}
   $address =  $output->results[0]->formatted_address; 
   $re = array('result' => 1,'msg'	=>"Address",'details'=>$address,'city_name'=>$city);
}
else
{
   
   $re = array('result' => 0,'msg'	=>"Required Field Missing");
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>