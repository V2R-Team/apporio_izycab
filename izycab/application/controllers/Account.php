<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Account extends REST_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Useraccountmodel');
        $this->load->model('Googlemodel');
    }

    function Signup_post()
    {
        $first_name = $this->post('first_name');
        $lastname =  $this->post('last_name');
        $user_phone = $this->post('phone');
        $user_password = $this->post('password');
        $user_email = $this->post('user_email');
        if($first_name != "" && $user_phone != ""  && $user_password != "" && $user_email != ""){
            $user_name = $first_name." ".$lastname;
            $query = $this->Useraccountmodel->check_user($user_phone,$user_password);
            if($query == 0){
                $data = $this->Useraccountmodel->check_phone($user_phone);
                if($data == 0) {
                    $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
                    $data=$dt->format('M j');
                    $day=date("l");
                    $date=$day.", ".$data ;
                    $user_signup_date = date("Y-m-d");
                    $data = array(
                        'user_name' => $user_name,
                        'user_phone' => $user_phone,
                        'user_password' => $user_password,
                        'register_date'=>$date,
                        'user_email'=>$user_email,
                        'user_signup_date'=>$user_signup_date
                    );
                    $this->Email($user_email,$user_name);
                    $text = $this->Useraccountmodel->signup($data);
                    $this->response([
                        'result' => 1,
                        'message' => "Signup Succusfully!!",
                        'details' => $text
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $user_id = $data['user_id'];
                    $data = array(
                        'user_name' => $user_name,
                        'user_password' => $user_password,
                        'user_email'=>$user_email
                    );
                    $text = $this->Useraccountmodel->update_profile($data,$user_id);
                    $this->response([
                        'result' => 1,
                        'message' => "Signup Succusfully!!",
                        'details' => $text
                    ], REST_Controller::HTTP_CREATED);
                }
            }
            else{
                $this->response([
                    'result' => 0,
                    'message' => "Phone already registerd!!"
                ], REST_Controller::HTTP_CREATED);
            }
        }
        else{
            $this->response([
                'result' => 0,
                'message' => "Required fields missing!!"
            ], REST_Controller::HTTP_CREATED);
        }
    }

     function Edit_Profile_post()
    {
        $user_name = $this->post('user_name');
        $user_email = $this->post('user_email');
        $user_id = $this->post('user_id');
        if($user_name != "" && $user_email != "" && $user_id != ""){
            $query = $this->Useraccountmodel->check_user_id($user_id);
            if(!empty($query)){
                $data = array(
                    'user_name' => $user_name,
                    'user_email' => $user_email,
                );
                $text = $this->Useraccountmodel->update_profile($data,$user_id);
                if(!empty($_FILES['user_image']['name'])){
                    $config = [
                        'upload_path' => './uploads/',
                        'allowed_types' => '*'
                    ];
                    $this->load->library('upload', $config);
                    $this->upload->do_upload('user_image');
                    $data = $this->upload->data();
                    $image = base_url("uploads/" . $data['raw_name'] . $data['file_ext']);
                    $data = array('user_image'=>$image);
                    $text = $this->Useraccountmodel->update_profile($data,$user_id);
                }
                $this->response([
                    'result' => 1,
                    'message' => "Profile Updated",
                    'details' => $text
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                                'result' => 0,
                                'message' => "No Record Found!!"
                            ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                            'result' => 0,
                            'message' => "Required fields missing!!"
                        ], REST_Controller::HTTP_CREATED);
        }
    }

    function Logout_post()
    {
        $user_id = $this->post('user_id');
        $unique_id = $this->post('unique_id');
        if (!empty($user_id) && !empty($unique_id))
        {
            $this->Useraccountmodel->logout($user_id,$unique_id);
                $this->response([
                    'result' => 1,
                    'message' => "Logout Successfully"
                ], REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                            'result' => 0,
                            'message' => "Required fields missing!!"
                        ], REST_Controller::HTTP_CREATED);
        }
    }

    public function phone_post()
    {
        $user_phone = $this->post('phone');
        if($user_phone != "")
        {

            $data = $this->Useraccountmodel->check_phone2($user_phone);
            if($data)
            {
                $this->response([
                                'result' => 1,
                                'message' => "user details",
                                 'details' => $data
                            ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                                    'result' => 0,
                                    'message' => "phone number not register!!"
                                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                                'result' => 0,
                                'message' => "Required fields missing!!"
                            ], REST_Controller::HTTP_CREATED);
        }
    }

    public function update_email_post()
    {
        $user_id = $this->post('user_id');
        $email = $this->post('email');
        if($user_id != "" && $email != "")
        {
            $data = $this->Useraccountmodel->update_email($email,$user_id);
            if($data)
            {
                $this->response([
                                'result' => 1,
                                'message' => "email update",
                                 'details'=>$data
                            ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'result' =>0,
                    'message' => "wrong user id"
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'result' =>0,
                'message' => "Required fields missing!!"
            ], REST_Controller::HTTP_CREATED);
        }
    }

    public function Login_post(){
        $user_phone = $this->post('phone');
        $user_password = $this->post('password');
        if($user_phone != "" && $user_password != "" ){
            $query = $this->Useraccountmodel->login($user_phone,$user_password);
            $id = $query['user_id'];
            $this->Useraccountmodel->online($id);
            if($query){
                $this->response([
                    'result' =>1,
                    'message' => "Login Succusfully!!",
                    'value'=>1,
                    'details'=>$query
                ], REST_Controller::HTTP_CREATED);
            }
            else{

                $data = $this->Useraccountmodel->check_phone($user_phone);
                if($data)
                {
                    $pass = "";
                    $text = $this->Useraccountmodel->phone($user_phone,$pass);
                    if($text)
                    {
                        $this->response([
                            'result' =>0,
                            'message' => "password does not match!!",
                            'value'=>2
                        ], REST_Controller::HTTP_CREATED);
                    }else {
                        $this->response([
                            'result' =>0,
                            'message' => "password does not match!!",
                            'value'=>0
                        ], REST_Controller::HTTP_CREATED);
                    }
                }else {
                    $this->response([
                                    'result' =>0,
                                    'message' => "user does not exsit!!",
                                    'value'=>4
                                ], REST_Controller::HTTP_CREATED);
                }
            }
        }
        else{
            $this->response([
                                'result' =>0,
                                'message' => "Required fields missing!!"
                            ], REST_Controller::HTTP_CREATED);
        }
    }

    public function Change_password_post()
    {
        $user_id = $this->post('user_id');
        $old_password = $this->post('old_password');
        $new_password = $this->post('new_password');
        if($user_id != "" && $old_password != "" && $new_password != ""){
            $data = $this->Useraccountmodel->change_password($user_id,$old_password,$new_password);
            if($data)
            {
                $this->response([
                    'result' =>1,
                    'message' => "password updated"
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'result' =>0,
                    'message' => "old password wrong"
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                            'result' =>0,
                            'message' => "Required fields missing!!"
                        ], REST_Controller::HTTP_CREATED);
        }
    }

    public function facebook_signup_post()
    {
        $facebook_id = $this->post('facebook_id');
        $facebook_mail = $this->post('facebook_mail');
        $facebook_image = $this->post('facebook_image');
        $facebook_firstname = $this->post('facebook_firstname');
        $facebook_lastname = $this->post('facebook_lastname');
        $phone = $this->post('phone');
        $first_name = $this->post('first_name');
        $last_name = $this->post('last_name');

        if($facebook_id != "" && $facebook_mail != "" && $facebook_image != "" && $facebook_firstname != "" && $phone != "" && $first_name != "")
        {

            $data = $this->Useraccountmodel->check_phone($phone);
            if(empty($data))
            {
                $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
                $data=$dt->format('M j');
                $day=date("l");
                $date=$day.", ".$data;
                $user_signup_date = date("Y-m-d");
                $text = array(
                    'facebook_id'=>$facebook_id,
                    'facebook_mail'=>$facebook_mail,
                    'facebook_image'=>$facebook_image,
                    'facebook_firstname'=>$facebook_firstname,
                    'facebook_lastname'=>$facebook_lastname,
                    'user_phone'=>$phone,
                    'user_name' => $first_name." ".$last_name,
                    'user_signup_type'=>2,
                    'register_date'=>$date,
                    'user_signup_date'=>$user_signup_date
                );

                $query = $this->Googlemodel->facebook_signup($text);
                $this->response([
                    'result' =>1,
                    'msg' => "Facebook Signup Successfully",
                    'details'=>$query
                ], REST_Controller::HTTP_CREATED);
            }else{
                $text = array(
                    'facebook_id'=>$facebook_id,
                    'facebook_mail'=>$facebook_mail,
                    'facebook_image'=>$facebook_image,
                    'facebook_firstname'=>$facebook_firstname,
                    'facebook_lastname'=>$facebook_lastname,
                    'user_name' => $first_name." ".$last_name
                );

                $query = $this->Googlemodel->facebook_updated($text,$phone);
                $this->response([
                                'result' =>1,
                                'msg' => "Details Updated",
                                'details'=>$query
                            ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                                'result' =>0,
                                'msg' => "Required field Missing"
                            ], REST_Controller::HTTP_CREATED);
        }
    }

    public function facebook_login_post()
    {
        $facebook_id = $this->input->post('facebook_id');

        if($facebook_id != ""){
            $query = $this->Googlemodel->facebook_login($facebook_id);
            if(empty($query))
            {
                $this->response([
                                'result' =>0,
                                'msg' => "Wrong Facebook Id"
                            ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                                'result' =>1,
                                'msg' => "login successfully",
                                'details'=>$query
                            ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                            'result' =>0,
                            'msg' => "Required field Missing"
                        ], REST_Controller::HTTP_CREATED);
        }
    }

    public function google_signup_post()
    {
        $google_id = $this->post('google_id');
        $google_name = $this->post('google_name');
        $google_mail = $this->post('google_mail');
        $google_image = $this->post('google_image');
        $phone = $this->post('phone');
        $first_name = $this->post('first_name');
        $last_name = $this->post('last_name');

        if($google_id != "" && $google_name != "" && $google_mail != "" && $google_image != "" && $phone != "" && $first_name != "")
        {

            $data = $this->Useraccountmodel->check_phone($phone);

            if(empty($data))
            {
                $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
                $data=$dt->format('M j');
                $day=date("l");
                $date=$day.", ".$data ;
                $user_signup_date = date("Y-m-d");
                $text = array(
                    'google_id'=>$google_id,
                    'google_name'=>$google_name,
                    'google_mail'=>$google_mail,
                    'google_image'=>$google_image,
                    'user_phone'=>$phone,
                    'user_name' => $first_name." ".$last_name,
                    'user_signup_type'=>3,
                    'register_date'=>$date,
                    'user_signup_date'=>$user_signup_date
                );
                $query = $this->Googlemodel->google_signup($text);

                $this->response([
                    'result' =>1,
                    'msg'=>"Google Signup Successfully",
                    'details'=>$query
                ], REST_Controller::HTTP_CREATED);
            }else{
                $text = array(
                    'google_id'=>$google_id,
                    'google_name'=>$google_name,
                    'google_mail'=>$google_mail,
                    'google_image'=>$google_image,
                    'user_name' => $first_name." ".$last_name
                );

                $query = $this->Googlemodel->google_update($text,$phone);
                $this->response([
                    'result' =>1,
                    'msg'=>"Detail updated",
                    'details'=>$query
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'result' =>0,
                'msg'=>"Required field Missing"
            ], REST_Controller::HTTP_CREATED);
        }
    }

    public function google_login_post()
    {
        $google_id = $this->post('google_id');

        if($google_id != ""){
            $query = $this->Googlemodel->google_login($google_id);
            if(empty($query))
            {
                $this->response([
                                    'result' =>0,
                                    'msg'=>"Wrong Google Id"
                                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                                    'result' =>1,
                                    'msg'=>"login successfully",
                                    'details'=>$query
                                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                            'result' =>0,
                            'msg'=>"Required field Missing"
                        ], REST_Controller::HTTP_CREATED);
        }
    }

    public function image_post()
    {
        $user_id = $this->post('user_id');
        if(!empty($_FILES["profile_image"]['name'])) {
            $condition1 = "";
            $ext  = strtolower(substr($_FILES["profile_image"]['name'], -4));
            $ext  = $ext == "jpeg" ? ".jpg" : $ext;
            $config['upload_path']    = 'images';
            $config['allowed_types']  = 'PNG|JPG|png|jpg|PDF|pdf';
            $config['overwrite']      = TRUE;
            $config['encrypt_name']   = FALSE;
            $config['remove_spaces']  = TRUE;
            if( !is_dir($config['upload_path']) ) mkdir($config['upload_path'], 0755, true);
            $condition['user_id'] = $user_id;
            $config['file_name'] = $user_id.time().$ext;
            $this->load->library('upload');
            $this->upload->initialize($config);
            $this->upload->do_upload('profile_image');
            $this->Googlemodel->update("user", array("user_image" => "images/".$user_id.time().$ext),$condition);
            $query = $this->Googlemodel->user($user_id);
            $this->response([
                            'result' =>1,
                            'msg'=>"image updated",
                             'details'=>$query
                        ], REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                            'result' =>0,
                            'msg'=>"image not updated"
                        ], REST_Controller::HTTP_CREATED);
        }
    }

    public function Email($user_email,$user_name)
    {
		$subject = "Taxi App Registration";
        $htmlContent = '<html>
			<head>
				<title>'.$subject.' </title>
			</head>
			<body>
				<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="padding:24px;background-color:#34495e">
					<tbody>
						<tr>
							<td>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="padding:30px 30px 30px 30px;background-color:#fafafa">
									<tbody>
									
									<tr>
										<td  style="font-size:28px;font-family:Arial,Helvetica,sans-serif;color:#34495e; text-align:center; padding-bottom:30px;" ><b> Taxi App</b></td>
									</tr>
									<tr>
										<td style="font-size:12px;font-family:Arial,Helvetica,sans-serif;color:#34495e" colspan="2"> Dear <strong>'.$user_name.'</strong>, </td>
									</tr>
									<tr>
										<td height="10" colspan="2"> </td>
									</tr>
									<tr>
									
										<td style="font-size:12px;text-align:justify;line-height:1.4;font-family:Arial,Helvetica,sans-serif;color:#34495e" colspan="2">	 	 
										<strong> We are happy to inform you that your registration for Taxi App is Succesfull. We assure you of our best services at all times .
 </strong>
										<br>
										
										
										</td>
									</tr>
									<tr>
										<td height="10" colspan="2"> </td>
									</tr>
									<tr>
										<td height="24" colspan="2">
											We Hope you enjoyed your experience using our service.
										</td>
									</tr>
									
									<tr>
										<td height="30" style="border-bottom:1px solid #eaedef" colspan="2"> </td>
									</tr>
									<tr>
										<td height="12" colspan="2"> </td>
									</tr>
									<tr>
										<td colspan="3" align="center">
											<span class="HOEnZb"> <font color="#888888"> </font> </span>
											<span class="HOEnZb"> <font color="#888888"> </font> </span>
											<table width="100%">
												<tbody>
													<tr>
														<td align="center">
															Thanks & Regards,
															<br> <br>
															The Taxi App Team.
														</td>
													</tr>
												</tbody>
											</table>
											<span class="HOEnZb"><font color="#888888"> </font> </span>
										</td>
									</tr>
								</tbody>
							</table>
							<span class="HOEnZb"><font color="#888888"> </font></span>
						</td>
					</tr>
				</tbody>
			</table>
			</body>
		</html>';
        $config['mailtype'] = 'html';
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->to($user_email);
        $this->email->from('hello@apporioinfolabs.com','Izycab');
        $this->email->subject('Taxi App Registration');
        $this->email->message($htmlContent);
        $this->email->send();
    }
}
