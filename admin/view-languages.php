<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query='SELECT * FROM table_languages';
$result=$db->query($query);
$list=$result->rows;

    $query="select * from languages";
    $result = $db->query($query);
    $list1=$result->rows;
    
if(isset($_POST['savechanges']))
{
    $query2="UPDATE table_languages SET language_name='".$_POST['languageName']."' where language_id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=view-languages");
}


if(isset($_GET['status']) && isset($_GET['id']))
{
$query1="UPDATE table_languages  SET language_status='".$_GET['status']."' WHERE language_id='".$_GET['id']."'";
$db->query($query1);
$db->redirect("home.php?pages=view-languages");
}
?>


<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Language Management  <a href="home.php?pages=add-languages" data-toggle="tooltip" title="Add Language" class="btn btn-primary add_btn"><i class="fa fa-plus"></i></a></h3>
            <span class="tp_rht">
            <!--<a href="home.php?pages=add-languages" class="btn btn-default btn-lg" id="add-button"  role="button">Add Language</a>-->
           
      </span>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>

                                        <th width="5%">S.No</th>
                                        <th>Language Name</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $count=1; foreach($list as $data){ ?>
                                        <tr>
                                            <td><?php echo $data['language_id'];?></td>
                                            <td>
                                                <?php
                                                echo $data['language_name'];
                                                ?>
                                            </td>


                                            <?php
                                            if($data['language_status']==1) {
                                                ?>
                                                <td class="">
                                                    <label class="label label-success" > Active</label>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="">
                                                    <label class="label label-default" > Deactive</label>
                                                </td>
                                            <?php }?>
                                            <td>
                                            <span data-target="#<?php echo $data['language_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
                                            
                                              <?php if($data['language_status']==1){ ?>
                                            
                                            <a data-original-title="Active" class="btn menu-icon btn_eye" href="home.php?pages=view-languages&status=2&id=<?php echo $data['language_id']?>"> <i class="fa fa-eye"></i> </a>
                                            <?php } else { ?>
                                            <a data-original-title="Inactive" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_eye_dis" href="home.php?pages=view-languages&status=1&id=<?php echo $data['language_id']?>"> <i class="fa fa-eye-slash"></i> </a>
                                            <?php } ?>
                                            <!--<button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#<?php echo $data['language_id'];?>"  ></button></td>-->
                                        </tr>
                                        <?php $count++; }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>
<?php foreach($list as $data){?>
<div class="modal fade" id="<?php echo $data['language_id'];?>" role="dialog">
  <div class="modal-dialog"> 
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit Language Details</h4>
      </div>
      <form  method="post"  onSubmit="return validatelogin()">
        <div class="modal-body">
         
                        <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                     <label for="field-3" class="control-label">Language Name</label>
                                    <select class="form-control" name="languageName" id="languageName">
                                        <option value="">--Please Select Language--</option>
                                        <?php foreach($list1 as $language){ ?>
                                            <option value="<?=  $language['name'];?>" <?php if($data['language_name'] == $language['name']){ ?> selected <?php } ?>><?php echo $language['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                 </div>
			            </div>
			          </div>
                            
                            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
          <button type="submit" name="savechanges" value="<?php echo $data['language_id'];?>" class="btn btn-info">Save Changes</button>
        </div>
      </form>
    </div>
    
  </div>
</div>
<?php }?>

</section>
<!-- Main Content Ends -->

</body></html>