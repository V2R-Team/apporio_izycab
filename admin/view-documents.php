<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query="SELECT * FROM table_document_list INNER JOIN city ON table_document_list.city_id=city.city_id  INNER JOIN table_documents ON table_document_list.document_id = table_documents.document_id";
$result=$db->query($query);
$list=$result->rows;
$a = array();
foreach($list as $key=>$value)
{
            $a[$value['city_id']]['city_id']=$value['city_id'];
            $a[$value['city_id']]['city_name']=$value['city_name'];
            $a[$value['city_id']]['document_name'][] = $value['document_name'];
}

?>


<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Fare Card</h3>
            <span class="tp_rht">
              <a href="home.php?pages=add-category-document" data-toggle="tooltip" title="Add Documents" class="btn btn-primary add_btn"><i class="fa fa-plus"></i></a>
      </span>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>

                                        <th width="5%">S.No</th>
                                        <th>City Name</th>
                                        <th>Documents</th>
                                        <th>Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; 
                                    foreach($a as $data){ ?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $data['city_name'];?></td>
                                            <td><?php echo implode(",",$data['document_name']);?></td>
                                            <td><a data-original-title="Edit" href="home.php?pages=edit-documents&id=<?=$data['city_id']?>" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a>

                                        </tr>
                                    <?php 
                                    $i++;
                                    }
                                     ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>

</section>
<!-- Main Content Ends -->

</body></html>