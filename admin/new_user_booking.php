<?php
include('common.php');
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']))
{
    $db->redirect("index.php");
}
$query1="select * from car_type WHERE car_admin_status=1";
$result1 =$db->query($query1);
$list1=$result1->rows;
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBE6xdPzQ3njIxhMN0Vj_hLURTE9ZROT2Y&v=3.exp&libraries=places"></script>

<script>
    function initialize() {

        var input = document.getElementById('pickup_location');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
    function initialize() {

        var input = document.getElementById('drop_location');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script type="text/javascript">
    function Validate() {
        var user_name = document.getElementById("user_name").value;
        var car_type_id = document.getElementById("car_type_id").value;
        var pickup_location = document.getElementById("pickup_location").value;
        var drop_location = document.getElementById("drop_location").value;
        var user_email = document.getElementById("user_email").value;
        var user_phone = document.getElementById("user_phone").value;
        if (user_name == "")
        {
            alert("Enter Username");
            return false;
        }

        if (user_email == "")
        {
            alert("Enter Useremail");
            return false;
        }
        if (user_phone == "")
        {
            alert("Enter Phone Number");
            return false;
        }
        if (car_type_id == "")
        {
            alert("Select Car Type.");
            return false;
        }
        if (pickup_location == "")
        {
            alert("Enter Pickup Location");
            return false;
        }
        if (drop_location == "")
        {
            alert("Enter Drop Location");
            return false;
        }


    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Book Ride</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form" >
                        <form class="cmxform form-horizontal tasi-form"  method="post" action="new_user_booking_form_submited.php" onSubmit="return Validate()">


                            <div class="form-group total red">
                                <label class="control-label col-lg-2">User Name*</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="User Name" name="user_name" id="user_name" >
                                </div>
                            </div>

                            <div class="form-group total red">
                                <label class="control-label col-lg-2">User Eamil*</label>
                                <div class="col-lg-10">
                                    <input type="email" class="form-control" placeholder="User Eamil" name="user_email" id="user_email" >
                                </div>
                            </div>

                            <div class="form-group total red">
                                <label class="control-label col-lg-2">Phone*</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Phone number With Country Code" name="user_phone" id="user_phone" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Car Type</label>
                                <div class="col-lg-10">
                                    <select class="form-control" name="car_type_id" id="car_type_id">
                                        <option value="">--Please Select Vehicle--</option>
                                        <?php foreach($list1 as $carbrand){ ?>
                                            <option value="<?php echo $carbrand['car_type_id'];?>"><?php echo $carbrand['car_type_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group total green">
                                <label class="control-label col-lg-2">Pickup Location</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Pickup Location" name="pickup_location" id="pickup_location">

                                </div>
                            </div>

                            <div class="form-group total green">
                                <label class="control-label col-lg-2">Drop Location</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Drop Location" name="drop_location" id="drop_location" value="">

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="manual" value="Manual" >
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="auto" value="Auto" style="margin-left: 20px">
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

</section>
<!-- Main Content Ends -->

</body>
</html>
