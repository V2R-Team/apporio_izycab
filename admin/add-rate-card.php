<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$query="select * from city";
$result = $db->query($query);
$list=$result->rows;

$query="select * from car_type";
$result = $db->query($query);
$list1=$result->rows;

if(isset($_POST['save'])) {
    $query = "select * from price_card WHERE city_id='".$_POST['city_id']."' AND car_type_id='".$_POST['car_type_id']."'";
    $result = $db->query($query);
    $list = $result->row;
    if(empty($list))
    {
        $query1="INSERT INTO price_card (distance_unit,currency,city_id,car_type_id,base_distance,base_distance_price,base_price_per_unit,free_waiting_time,wating_price_minute,free_ride_minutes,price_per_ride_minute,commission) VALUES('".$_POST['distance']."','".$_POST['currency']."','".$_POST['city_id']."','".$_POST['car_type_id']."','".$_POST['base_distance']."','".$_POST['base_distance_price']."','".$_POST['base_price_per_unit']."','".$_POST['free_waiting_time']."','".$_POST['wating_price_minute']."','".$_POST['free_ride_minutes']."','".$_POST['price_per_ride_minute']."','".$_POST['commission']."')";
        $db->query($query1);
        $msg = "Rate Card Save Successfully";
        echo '<script type="text/javascript">alert("'.$msg.'")</script>';
        $db->redirect("home.php?pages=add-rate-card");
    }else{
        $msg = "Rate Card For This Car Already Added";
        echo '<script type="text/javascript">alert("'.$msg.'")</script>';
        $db->redirect("home.php?pages=add-rate-card");
    }
}
?>

<script type="text/javascript">

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    function getId(val) {
        $.ajax({
            type: "POST",
            url: "distance.php",
            data: "city_id="+val,
            success:
                function(value){
                    var data = value.split(",");
                    $('#distance').val(data[0]);
                    $('#currency').val(data[1]);
                }
        });
    }
</script>
<script type="text/javascript">
    function Validate() {
        if(document.getElementById("city_id").value == "")
        {
            alert("Select City");
            return false;
        }
        if(document.getElementById("car_type_id").value == "")
        {
            alert("Select Car Type");
            return false;
        }
        if(document.getElementById("base_distance").value == "")
        {
            alert("Enter Base Ride Distance");
            return false;
        }
        if(document.getElementById("base_distance_price").value == "")
        {
            alert("Enter Base Ride Distance Price");
            return false;
        }
        if(document.getElementById("base_price_per_unit").value == "")
        {
            alert("Enter Price Per Unit After Base Distance");
            return false;
        }
        if(document.getElementById("free_waiting_time").value == "")
        {
            alert("Enter Free Wating Time");
            return false;
        }
        if(document.getElementById("wating_price_minute").value == "")
        {
            alert("Enter Wating Price Per Minute");
            return false;
        }
        if(document.getElementById("free_ride_minutes").value == "")
        {
            alert("Please Enter Free Ride Minute");
            return false;
        }
        if(document.getElementById("price_per_ride_minute").value == "")
        {
            alert("Please Enter Price Ride Per Minute");
            return false;
        }
        if(document.getElementById("commission").value == "")
        {
           alert("Enter Commision");
            return false;
        }
    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Rate Card</h3>
        
      <span class="tp_rht">
           <a href="home.php?pages=view-rate-card" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
       </span>
    </div>
    <form class="cmxform form-horizontal tasi-form"  method="post"  >
        <div class="row">
            <div class="col-sm-12">

                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="form" >

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Choose City*</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="city_id" id="city_id" onchange="getId(this.value);">
                                        <option value="">--Please Select City Name--</option>
                                        <?php foreach($list as $cityname): ?>
                                            <option id="<?php echo $cityname['city_id'];?>"  value="<?php echo $cityname['city_id'];?>"><?php echo $cityname['city_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Distance Unit</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" placeholder="Distance Unit " name="distance" id="distance" readonly>
                                </div>
                            </div>


                            <div class="form-group ">
                                <label class="control-label col-lg-2">Currency</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" placeholder="Currency " name="currency" id="currency" readonly>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Choose Car Type Name*</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="car_type_id" id="car_type_id" required>
                                        <option value="">--Please Select Car Type--</option>
                                        <?php foreach($list1 as $cartype): ?>
                                            <option id="<?php echo $cartype['car_type_id'];?>"  value="<?php echo $cartype['car_type_id'];?>"><?php echo $cartype['car_type_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            
                            
                            <div class="form-group ">
                                   <label class="control-label col-lg-2">Company Commission</label>
                                    <div class="col-lg-6">
                                          <input type="text" class="form-control" placeholder="Company Commission" name="commission" onkeypress="return isNumber(event)" id="commission" >
                                    </div>
                             </div>


                            <div class="page-title">
                                <h3 class="title">Ride Distance charges</h3>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">

                                        <div class="panel-body">
                                            <div class="form" >

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Base Ride Distance (Miles/KM)</label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Base Ride Distance (Miles/KM)" name="base_distance" onkeypress="return isNumber(event)" id="base_distance">
                                                    </div>
                                                </div>

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Base Ride Distance Charges </label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Base Ride Distance Charges" name="base_distance_price" onkeypress="return isNumber(event)" id="base_distance_price" >
                                                    </div>
                                                </div>

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Price Per Unit</label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Price Ride Per unit After Base Distance" name="base_price_per_unit" onkeypress="return isNumber(event)" id="base_price_per_unit" required>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="page-title">
                                <h3 class="title">Waiting Charges</h3>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">

                                        <div class="panel-body">
                                            <div class="form" >

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Free waiting Time</label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Free waiting Time" name="free_waiting_time" onkeypress="return isNumber(event)" id="free_waiting_time" required>
                                                    </div>
                                                </div>

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Waiting Price Per Minute</label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Waiting Price Per Minute After Free Waiting Time" name="wating_price_minute" onkeypress="return isNumber(event)" id="wating_price_minute" required>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="page-title">
                                <h3 class="title">Ride Time Charges</h3>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">

                                        <div class="panel-body">
                                            <div class="form" >

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2">Free Ride Minute</label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Free Ride Minute" name="free_ride_minutes" onkeypress="return isNumber(event)" id="free_ride_minutes" required>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-lg-2">Price Per Ride Minute</label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Price Per Minute After Free Ride Time" onkeypress="return isNumber(event)" name="price_per_ride_minute" id="price_per_ride_minute" required>
                                                    </div>
                                                </div>       
                                            </div>
 <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-10">
                                                        <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save"  onclick="Validate()" value="Add Rate Card" >
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</section>
</body>
</html>
