<!DOCTYPE html>
<html>
<head>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 70%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
<div id="map"></div>

<script src="https://www.gstatic.com/firebasejs/3.0.5/firebase.js"></script>
<script>
		var config = {
			apiKey: "AIzaSyDaTYBLUlXlWnWi0Q7ine8DTvw1d9m4djY",
			authDomain: "apporio-taxi.firebaseapp.com",
			databaseURL: "https://apporio-taxi.firebaseio.com",
			projectId: "apporio-taxi",
			storageBucket: "apporio-taxi.appspot.com",
			messagingSenderId: "316763323278"
			};
			firebase.initializeApp(config);
		var firebases = firebase.database().ref("Drivers_A/");
                function initMap() 
                {
                    map = new google.maps.Map(document.getElementById('map'), 
                    {
                        zoom: 10,
                        center: new google.maps.LatLng(28.4120558, 77.0433644),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                    
                   
                    firebases.on("child_added", function(snapshot) {
                        // Get latitude and longitude from the cloud.
                        var newPosition = snapshot.val();
			var name = newPosition.driver_name;
			var lat = newPosition.driver_current_latitude;
			var longitude = newPosition.driver_current_longitude;
			var locations = [name,lat,longitude];
                      
                       var infowindow = new google.maps.InfoWindow(), marker, i;
                       var image = 'http://apporioinfolabs.com/apporiotaxi_newadmin/admin/img/icon.png';
			
			marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[1], locations[2]),
			map: map,
			icon: image
			});
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
			infowindow.setContent(locations[0]);
			infowindow.open(map, marker);
			}
			})(marker, i));
                    }
                )}
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnJYaYEdOvK48PVHbYa5jjQ8H2EaYmKe8&libraries=visualization&callback=initMap">
</script>
</body>
</html>